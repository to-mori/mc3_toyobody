
//moricon3_toyobody

#include <xc.h>
#define _XTAL_FREQ 8000000
#pragma config BOREN = OFF
#pragma config FOSC = INTRC_NOCLKOUT
#pragma config FCMEN = OFF
#pragma config MCLRE = OFF
#pragma config WDTE = OFF
#pragma config LVP = OFF
#pragma config PWRTE = ON

//timer
#define timerH cf
#define timerL 2c

//input
#define start_sw RB1
#define stop_sw RB2
//#define lock_signal RB5

//output
//#define sertif_sw_pc RC4
#define motor_in1 RC5
#define motor_in2 RC6
#define count_output_pc RA3

//LED,BUZZER
#define power_led RC0
#define lock_drive_led RC2
#define unlock_drive_led RA6
//#define lock_signal_led RC1
//#define errow_led RC3
//#define BUZZER RA7

//counter
#define LockCounter 20                                  //lock time counter
#define UnlockCounter 20                                //unlock time counter
#define LockIntervalock_time_counterounter 5          //interval time counter
#define UnlockIntervalock_time_counterounter 5        //interval time counter
#define NumberOfTest 50000                             //number of test

static void pic_init();
static void timer_resetting();
//static void lock_signal_detection();
static void stop_mode();
static void lock_drive_states();
static void lock_interval_states();
static void unlock_drive_states();
static void unlock_interval_states();
static void manual_stop();
static void auto_stop();
//static void irregular_stop();
//static void irregular_stop_mode();

int mode;                                   //mode
    //mode 0:stop
    //mode 1:operating
    //mode 2:irregular stop
int state;                                  //state flag
    //state 1:lock action
    //state 2:lock action -> interval
    //state 3:unlock action
    //state 4:unlock action -> interval
int lock_time_counter;      //lock time counter
int interval_time_counter;  //interval time counter
int unlock_time_counter;    //unlock time counter
//int drive_counter;          //drive counter
//int lock_signal_counter;    //lock output counter
int test_counter;           //test counter
//int lock_signal_bfr;        //lock signal before


//====================main fanction====================//
void main(void)
{
    //==========pic initialize==========//
    pic_init();
    //==========main loop==========//
    while(1)                //main loop
    {
        power_led=1;        //power LED ON
    }
}


//====================timer1 interrupt====================//
void interrupt A(void)
{
    //==========timer resetting==========//
    timer_resetting();

        switch(mode)                                            //switch(mode)
        {
            //==========mode stopping==========//
            case 0:                                             //case stop
                if(start_sw==0)                                 //if start sw ON
                {
                    stop_mode();
                }
                break;                                          //case stop break
            //==========mode operating==========//
            case 1:                                             //case operating
                    if(test_counter<=NumberOfTest)             //if test finish
                    {
                        if(stop_sw==1)                          //if stop sw OFF
                        {
                            switch(state)                       //switch(state)
                            {
                                case 1:                         //if lock action
                                    lock_drive_states();
                                    break;                      //case 1 break
                                case 2:                         //if normal -> interval
                                    lock_interval_states();
                                    break;                      //case 2 break
                                case 3:                         //if reversal
                                    unlock_drive_states();
                                    break;                      //case 3 break
                                case 4:                         //if lock -> interval
                                    unlock_interval_states();
                                    break;                      //case 4 break
                                default:
                                    break;                      //state other break
                            }
                        }
                        //=====manual stop=====//
                        else
                        {
                            manual_stop();
                        }
                    }
                    //=====auto stop=====//
                    else
                    {
                        auto_stop();
                    }
                break;                                   //case operating break
            default:
                break;
        }

    //==========timer1 flag clear==========//
    TMR1IF=0;                                            //timer1 flag clear
}


//====================user fanctions====================//

//==========PIC initialize==========//
static void pic_init()
{
    OSCCON=0b01110000;                                   //clock 8MHz
    CM1CON0=0b00000000;                                  //comparator OFF
    PORTA=0b00000000;                                    //portA output 0
    PORTB=0b00000000;                                    //portB output 0
    PORTC=0b00000000;                                    //portC output 0
    TRISA=0b00000000;                                    //portA input
    TRISB=0b00100110;                                    //portB input start_sw,2,5
    TRISC=0b00000000;                                    //portC input -

    ANSEL=0b00000000;                                    //nothing analog
    ANSELH=0b00000000;                                   //nothing analog

    WPUB=0b00100110;                                     //start_sw,2,5 internal pull up
    OPTION_REG=0b00000000;                               //pull up enable

    /*timer interrupt 1 setting*/
    T1CON=0x30;                                          //interval timer
    TMR1H=0xtimerH;                                      //timer H
    TMR1L=0xtimerL;                                      //timer L
    TMR1IE=1;                                            //interrupt ok
    TMR1ON=1;                                            //timer1 start

    PEIE=1;                                              //all interrupt ok
    GIE=1;                                               //all interrupt ok

    /*default*/
    mode=0;                                              //mode = stop
    state=0;                                             //state = standby
    lock_time_counter=0;                                 //lock time counter = 0
    unlock_time_counter=0;                               //unlock time counter = 0
    interval_time_counter=0;                             //interval time counter = 0
    //drive_counter=0;                                   //drive counter = 0
    //lock_signal_counter=0;                             //lock output counter = 0
    test_counter=0;                                      //test counter = 0
}

//==========timer_resetting==========//
static void timer_resetting()
{
    if(TMR1IF)
    {
        TMR1H=0xtimerH;                                  //timer H
        TMR1L=0xtimerL;                                  //timer L
    }
}

//==========stop_mode==========//
static void stop_mode()
{
    if(start_sw==0)                                     //if start sw ON
    {
        mode=1;                                         //mode = operating
        state=1;                                        //state = lock action
    }
}

//==========lock_drive_states==========//
static void lock_drive_states()
{
    if(lock_time_counter>=LockCounter)                  //if lock time count >= 500ms
    {
        motor_in1=0;                                    //motor stop
        motor_in2=0;
        lock_drive_led=0;                               //green LED OFF
        lock_time_counter=0;                            //lock time count reset
        count_output_pc=0;                              //count output OFF
        state=2;                                        //state = lock action -> interval
    }
    else
    {
        motor_in1=1;                                    //motor normal revolution
        motor_in2=0;
        lock_drive_led=1;                               //green LED ON
        lock_time_counter++;                            //lock time count +
        count_output_pc=0;                              //count output OFF
    }
}

//==========lock_interval_states==========//
static void lock_interval_states()
{
    if(interval_time_counter>=LockIntervalock_time_counterounter)               //if interval time count >=2000ms
    {
        motor_in1=0;                                    //motor stop
        motor_in2=0;
        interval_time_counter=0;                        //interval time count reset
        count_output_pc=0;                              //count output OFF
        state=3;                                        //state = reversal drive
    }
    else
    {
        motor_in1=0;                                    //motor stop
        motor_in2=0;
        interval_time_counter++;                        //interval time count +1
        count_output_pc=0;                              //count output OFF
    }
}

//==========unlock_drive_states==========//
static void unlock_drive_states()
{
    if(unlock_time_counter>=UnlockCounter)              //if unlock time count >=500ms
    {
        motor_in1=0;                                    //motor stop
        motor_in2=0;
        unlock_drive_led=0;                             //blue LED OFF
        unlock_time_counter=0;                          //unlock time count reset
        count_output_pc=0;                              //count output OFF
        state=4;                                        //state = unlock -> interval
    }
    else
    {
        motor_in1=0;                                    //motor reversal revolution
        motor_in2=1;
        unlock_drive_led=1;                             //blue LED ON
        unlock_time_counter++;                          //unlock time count +1
        count_output_pc=0;                              //count output OFF
    }
}

//==========unlock_interval_states==========//
static void unlock_interval_states()
{
    if(interval_time_counter>=UnlockIntervalock_time_counterounter)             //if interval time count >=2000ms
    {
        motor_in1=0;                                    //motor stop
        motor_in2=0;
        interval_time_counter=0;                        //interval time count reset
        test_counter++;                                 //test count +1
        count_output_pc=0;                              //count output OFF
        state=1;                                        //state = normal drive
    }
    else
    {
        motor_in1=0;                                    //motor stop
        motor_in2=0;
        interval_time_counter++;                        //interval time count +1
        count_output_pc=1;                              //count output ON
    }
}

//==========mamual_stop==========//
static void manual_stop()
{
    motor_in1=0;                                        //motor stop
    motor_in2=0;
    unlock_drive_led=0;lock_drive_led=0;                //blue,green LED
    mode=0;                                             //mode = stop
    lock_time_counter=0;
    interval_time_counter=0;
    unlock_time_counter=0;
    count_output_pc=0;                                  //count output OFF
    state=0;                                            //state = standby
}

//==========auto_stop==========//
static void auto_stop()
{
    motor_in1=0;                                        //motor stop
    motor_in2=0;
    unlock_drive_led=0;                                 //blue LED OFF
    unlock_time_counter=0;                              //unlock time count reset
    lock_time_counter=0;
    interval_time_counter=0;
    count_output_pc=0;                                  //count output OFF
    mode=0;                                             //mode = stop
}
